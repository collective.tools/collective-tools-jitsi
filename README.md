https://dev.to/noandrea/self-hosted-jitsi-server-with-authentication-ie7


dev.to
Self-hosted Jitsi server with authentication
Andrea Giacobino
7-8 minuter

As the Coronavirus spreads and more and more people are spending time at home, one of the most critical thing for many is to be able to keep in touch with family, friends and colleagues.

We at Ape Unit are used to remote and distributed work but the current situation meant that it was necessary to go above and beyond....

As we are involved in open source projects and since we already use Jitsi we opted to deploy an instance of Jitsi to use internally and since there have been a lot of demand on the Jitsi community forum about installation and configuration here is a step by step tutorial about how to run your own server.

Once you have completed the tutorial you will have your own Jisti server with authentication enabled.
Before you start

This tutorial assumes that you have basic knowledge about technical stuff in particular assumes that you are familiar with the linux shell, ssh, and dns.
The costs

The running cost of the setup varies depending on the server choice and the domain, in this case we'll be using a Hetzner cloud instance that starts from 2.49€/m

this being said, let's start!
#1. Setup a new server (with Hetzner)

Login (or create an account on Hetzner), then:

    go to the Cloud home
    choose the project you want (usually Default)
    Click on ADD SERVER

In the Create a Server page make sure to

    select the Ubuntu 18.04 as Image (point 2)
    add your public ssh key (point 7)

Then click on CREATE & BUY NOW

    If you add your ssh key, make sure that it is enable in the ssh configuration of your machine (more on that here). If you do not add your ssh key, you'll receive an email with the root password to the server that you will have to change on first login. As a side note, public key authentication is preferable then password.

The server creation is going to take a minute or less, once completed take note of the IP address of the server.
#2. Point a domain to the newly created server

for this step, we'll assume that you already have a domain registered (you can grab one on namecheap. For this tutorial we will use the domain apeunit.test for our domain.

In your domain DNS configuration add the records:

    type: A
    host: @
    value: IP Address (from the Hetzner server)

    NOTE: it might take a few minutes for the DNS to propagate

#3. Jitsi installation

Now that the server is up and running, let's set it up!
If everything before has been successful you should be able to login to your server using:

$ ssh root@apeunit.test
The authenticity of host 'apeunit.test (10.0.0.1)' can't be established.
ECDSA key fingerprint is SHA256:Q1rLmH7vuBalRJGv7sasTJy+ZtS3yOf4A34artGjUI.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'apeunit.test,10.0.0.1' (ECDSA) to the list of known hosts.
Welcome to Ubuntu 18.04.4 LTS (GNU/Linux 4.15.0-91-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage
root@server:~#

We can now proceed with Jitsi installation (documentation here)

First let's make sure that the system is up to date

apt update
apt upgrade -y

Then install nginx

Now we can add the Jitsi repositories and install the Jitsi components. During the installation, you will be prompted for a domain to use, when it happens enter the domain you choose/registered at #2. The second question you'll be asked is if generate a self signed certificate or provide your own; choose the default (generate a self signed certificate)

echo 'deb https://download.jitsi.org stable/' >> /etc/apt/sources.list.d/jitsi-stable.list
wget -qO -  https://download.jitsi.org/jitsi-key.gpg.key | apt-key add -
apt update && apt install jitsi-meet -y

And finally, lets ask let's encrypt for a certificate for our domain

/usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh

To verify that the installation was successful, open your browser and enter your domain in the address bar, Jitsi should be now up and running.

Once we have verified that the installation is working properly, it is time to configure the firewall so not to leave your server exposed. For firewall we are going to use ufw:

ufw allow ssh
ufw allow http
ufw allow https
ufw allow 10000/udp
ufw enable

#4 Enable authentication for Jitsi

Enabling the authentication requires only to edit three files:

/etc/jitsi/meet/apeunit.test-config.js
/etc/prosody/conf.avail/apeunit.test.cfg.lua
/etc/jitsi/jicofo/sip-communicator.properties

    NOTE: for the first two, change the apeunit.test part of the filename with your domain

The official documentation for secure domain explains the details of the changes, but here are the files already configured:

I've published the changed files here:

    /etc/jitsi/meet/apeunit.test-config.js
    /etc/prosody/conf.avail/apeunit.test.cfg.lua
    /etc/jitsi/jicofo/sip-communicator.properties

but they have to be changed according to your domain name, here is how. First set a variable with your domain name:

export JITSI_DOMAIN="your_domain.com"

then update the meet configuration file:

curl  https://gist.githubusercontent.com/noandrea/5ff6b212273af95103996c0e71f0cdf2/raw/22965f246c59bc149245554b6079db97794425bd/apeunit.test-config.js -s | \
sed  "s/apeunit.test/$JITSI_DOMAIN/g" \
> /etc/jitsi/meet/$JITSI_DOMAIN-config.js

then the prosody configuration file:

curl https://gist.githubusercontent.com/noandrea/5ff6b212273af95103996c0e71f0cdf2/raw/bb3e8a65582882dd3aeb4624d2522b244b949855/apeunit.test.cfg.lua -s | \
sed  "s/apeunit.test/$JITSI_DOMAIN/g" | \
sed  "s/JICOFO_SECRET/$(grep -e '^JICOFO_SECRET=.*' /etc/jitsi/jicofo/config | cut -d '=' -f2)/g" | \
sed  "s/TURN_SECRET/$(< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-8})/g" \
> /etc/prosody/conf.avail/$JITSI_DOMAIN.cfg.lua

and finally the sip communicator properties file:

echo "org.jitsi.jicofo.auth.URL=XMPP:$JITSI_DOMAIN" \
>> /etc/jitsi/jicofo/sip-communicator.properties

Once this part is completed it is now time to create your users, with the command prosodyctl register USER DOMAIN PASSWORD; in our example we can register the user jane with:

prosodyctl register jane $JITSI_DOMAIN janepwd

    Currently there is no option to make users register themselves, you will have to create users by command line. Also not all the users need to have a password, only the ones that open a room.

To apply the changes last thing we have to do is to restart Jitsi services:

service jicofo restart
service jitsi-videobridge2 restart
service prosody restart

That's it! Now you can enjoy your private chat server thanks to the wonderful Jitsi!
